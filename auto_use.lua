-- be careful, encoding is CP1251
script_author("THERION")

local sampev = require("samp.events")
local encoding = require("encoding")

--- Server defined parameters
local server_cooldown = {
   food = 5,
   meds = 5,
   drugs = 30	
}
local indicator = {
   text = "����� {FFFFFF}H {FFFF00}��� ������� ������ � ����������.",
   color  = "ffff00aa"
}

--- Settings
local script_on = true
local min_health = 45
local order = {
   { keyword = "food", cmd = "/eat"}, 
   { keyword = "meds", cmd = "/usemeds"}, 
   { keyword = "drugs", cmd = "/udc"}
}

--- Data container
local cooldown_state = {
   food = 0,
   meds = 0,
   drugs = 0
}

--- Try heal
local heal_thread = lua_thread.create_suspended(
function()
   wait(0)
   for _, data in pairs(order) do
      if os.clock() - cooldown_state[data.keyword] > server_cooldown[data.keyword] then
         sampSendChat(data.cmd)
         break
      end
   end
end)

local function limit_value(value, min, max)
   if value < min then value = min end
   if value > max then value = max end
   return value
end

local function is_in_array(array, value)
   for _, element in ipairs(array) do
      if value == element then
         return true
      end
   end
   return false
end

--- Hooks

-- catch cooldowns
sampev.onSendCommand = function(str)
   for _, data in pairs(order) do
      if str:find(data.cmd) then
         cooldown_state[data.keyword] = os.clock()
      end
   end
end

-- if player has critical health in vehicle
sampev.onSetPlayerHealth = function(health)
   if isCharInAnyCar(PLAYER_PED) and health < min_health then
      heal_thread:run()
   end
end

-- catch fall animation
sampev.onServerMessage = function(color, text)
   if bit.tohex(color) == indicator.color and text == indicator.text then
      heal_thread:run()
   end
end

function main()
   repeat wait(0) until isSampAvailable()

   print("disable: /autouse")
   sampRegisterChatCommand("autouse", function() thisScript():unload() end)

   wait(-1)
end