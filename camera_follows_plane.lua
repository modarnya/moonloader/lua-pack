local function on_every_frame()
   if not isCharInAnyPlane(PLAYER_PED) then
      return
   end

   local plane = storeCarCharIsInNoSave(PLAYER_PED)
   local plane_health = getCarHealth(plane)

   if plane_health >= 250.0 then
      return
   end

   setCameraBehindPlayer()
end

function main()
   while true do
      wait(0)
      on_every_frame()
   end
end