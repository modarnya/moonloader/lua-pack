script_authors("FYP", "hnnssy", "THERION")
script_description("allows you to change time & weather")

local CMD = "wt"

local MAX_WEATHER = 50
local MAX_TIME    = 24

-- prints out if user does not have some of the needed files
local function catch_exception(status, path)
   local msg = string.format('File not found or has errors: "%s". Shutting down..', path)
   assert(status, msg)
end

do -- Loading libraries
   local list = {
      memory     = "memory",
      inicfg     = "inicfg",
      imgui      = "mimgui",
   }

   local result = nil
   for var, path in pairs(list) do
      result, _G[var] = pcall(require, path)
      catch_exception(result, path)
   end
end

local function default()
   return {
      settings = {
         on = true,
         weather = 6,
         time = 12,
      }
   }
end

local ini = inicfg.load(default(), thisScript().name .. ".ini")

local win_state = imgui.new.bool(false)

local default_memory = nil
local old_weather, old_time = 1, 1
--- Functions
local function patch_samp_time(enable)
   local address = sampGetBase() + 0x9C0A0
	if enable then
		writeMemory(address, 4, 0x000008C2, true)
	else
		writeMemory(address, 4, default, true)
	end
end

local function get_weather()
   return memory.getint8(0xC81320, true)
end

local function switch_wt(on)
   patch_samp_time(on)
   local time    = on and ini.settings.time[0]    or old_time
   local weather = on and ini.settings.weather[0] or old_weather
   setTimeOfDay(time, 0)
   forceWeatherNow(weather)
end

onScriptTerminate = function(script, _)
	if script == thisScript() then
      switch_wt(false)
   end
end

function main()
   do -- save config
      if not doesDirectoryExist("moonloader\\config") then 
         createDirectory("moonloader\\config") 
      end

      if not doesFileExist(thisScript().name .. ".ini") then 
         inicfg.save(ini, thisScript().name .. ".ini") 
      end
   end

   repeat wait(0) until isSampAvailable()

   do -- get original data
      default_memory = readMemory(sampGetBase() + 0x9C0A0, 4, true)
      old_time, _ = getTimeOfDay()
      old_weather = get_weather()
   end

   imgui.to()
   
   if ini.settings.on[0] then -- patch samp time set loop and set weather if enabled
      patch_samp_time(true)
      forceWeatherNow(ini.settings.weather[0])
   end
   sampRegisterChatCommand(CMD, 
   function() 
      win_state[0] = not win_state[0] 
   end)

   while true do wait(0)
      if ini.settings.on[0] then
         setTimeOfDay(ini.settings.time[0], 0)
      end
   end
end


local new_frame = imgui.OnFrame(
function()
   return win_state[0] 
end,
function(player)
   imgui.apply_style()

   local flags = imgui.WindowFlags.NoResize + imgui.WindowFlags.NoTitleBar + imgui.WindowFlags.AlwaysAutoResize
   local win_pos = imgui.ImVec2(imgui.GetIO().DisplaySize.x / 4, imgui.GetIO().DisplaySize.y / 2)
   imgui.SetNextWindowPos(win_pos, imgui.Cond.FirstUseEver)

   imgui.Begin(thisScript().name, nil, flags)
      imgui.Text(thisScript().name)
      
      -- settings
      if imgui.Checkbox("Enabled", ini.settings.on) then
         switch_wt(ini.settings.on[0])
      end
      if imgui.InputInt("Weather", ini.settings.weather) then
         ini.settings.weather[0] = ini.settings.weather[0] % (MAX_WEATHER + 1)
         if ini.settings.on[0] then
            forceWeatherNow(ini.settings.weather[0])
         end
      end
      if imgui.InputInt("Time", ini.settings.time) then 
         ini.settings.time[0] = ini.settings.time[0] % (MAX_TIME + 1)
      end
      -- settings

      if imgui.Button("Save", imgui.ImVec2(224, 20)) then
         imgui.from()
         inicfg.save(ini, thisScript().name .. ".ini")
         imgui.to()
      end
   imgui.End()
end)

-- clickable text
imgui.Link = function(link, name, color, color_hovered)
   local size = imgui.CalcTextSize(name)
   local p = imgui.GetCursorScreenPos()
   local p2 = imgui.GetCursorPos()
   local result_btn = imgui.InvisibleButton("##".. link .. name, size)

   if result_btn then
      os.execute("explorer ".. link)
   end

   imgui.SetCursorPos(p2)
   if imgui.IsItemHovered() then
      imgui.TextColored(color_hovered, name)
   else
      imgui.TextColored(color, name)
   end
   
   return result_btn
end


imgui.to = function()
   ini.settings.on = imgui.new.bool(ini.settings.on)
   ini.settings.weather = imgui.new.int(ini.settings.weather)
   ini.settings.time = imgui.new.int(ini.settings.time)
end

imgui.from = function()
   ini.settings.on = ini.settings.on[0]
   ini.settings.weather = ini.settings.weather[0]
   ini.settings.time = ini.settings.time[0]
end

-- imgui style
imgui.apply_style = function()
   -- thx man with a moon profile picture
   imgui.SwitchContext()
   local colors = imgui.GetStyle().Colors
   colors[imgui.Col.Text]                    = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
   colors[imgui.Col.TextDisabled]            = imgui.ImVec4(1.00, 1.00, 1.00, 0.20)
   colors[imgui.Col.SliderGrab]              = imgui.ImVec4(0.90, 0.90, 0.90, 1.00)
   colors[imgui.Col.SliderGrabActive]        = imgui.ImVec4(0.70, 0.70, 0.70, 1.00)
   colors[imgui.Col.ScrollbarBg]             = imgui.ImVec4(0.60, 0.60, 0.60, 0.90)
   colors[imgui.Col.ScrollbarGrab]           = imgui.ImVec4(0.90, 0.90, 0.90, 1.00)
   colors[imgui.Col.ScrollbarGrabHovered]    = imgui.ImVec4(0.80, 0.80, 0.80, 1.00)
   colors[imgui.Col.ScrollbarGrabActive]     = imgui.ImVec4(0.70, 0.70, 0.70, 1.00)
   colors[imgui.Col.FrameBg]                 = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
   colors[imgui.Col.FrameBgHovered]          = imgui.ImVec4(0.20, 0.20, 0.20, 0.80)
   colors[imgui.Col.FrameBgActive]           = imgui.ImVec4(0.20, 0.20, 0.20, 0.60)
   colors[imgui.Col.CheckMark]               = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
   colors[imgui.Col.Button]                  = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
   colors[imgui.Col.ButtonHovered]           = imgui.ImVec4(0.15, 0.15, 0.15, 1.00)
   colors[imgui.Col.ButtonActive]            = imgui.ImVec4(0.10, 0.10, 0.10, 1.00)
   colors[imgui.Col.TextSelectedBg]          = imgui.ImVec4(0.80, 0.80, 0.80, 0.80)
end
