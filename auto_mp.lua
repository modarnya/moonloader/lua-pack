-- be careful, encoding is CP1251
script_author("THERION")
script_description("Anti-Shitshow")
script_dependencies("SAMP.lua")

local GALAXY_IP  = {"176.32.39.200", "176.32.39.199", "176.32.39.198"}
local SN_pattern = "������� �� .+%[(%d+)%]:"
local PM_pattern = "/pm %d %s"

local CMD = "automp"
local state = false
local mode = "calc"
local delay = 2.3

local ffi = require("ffi")
local ev = require("samp.events")

ffi.cdef[[
   typedef unsigned short WORD;
   
   typedef struct _SYSTEMTIME {
      WORD wYear;
      WORD wMonth;
      WORD wDayOfWeek;
      WORD wDay;
      WORD wHour;
      WORD wMinute;
      WORD wSecond;
      WORD wMilliseconds;
   } SYSTEMTIME, *PSYSTEMTIME;

   void GetLocalTime(PSYSTEMTIME lpSystemTime);
]]

-- prints message to SAMP Chat
local function log(msg)
   local format = "{FB4343}[%s]{FFFFFF}: %s{FFFFFF}."
   sampAddChatMessage(string.format(format, thisScript().name, msg), -1)
end

-- may be incorrect if 23.59 -> 00.01 stuff happens
local function get_raw_time()
   local time = ffi.new("SYSTEMTIME")
   ffi.C.GetLocalTime(time)
   local raw = time.wHour * 60 * 60 * 1000 + time.wMinute * 60 * 1000 + time.wSecond * 1000 + time.wMilliseconds
   return raw
end

local cmds = {
   on = function(_)
      state = true
      log("� ����� ����")
   end,
   off = function(_)
      state = false
      log("����")
   end,
   calc = function(_)
      mode = "calc",
      log('����������� "�����������", ������ ���������')
   end,
   ab = function(_)
      mode = "ab"
      log('����������� "��������� �������� ��� ���������", ������ ���������')
   end,
   delay = function(arg_list)
      local delay_unchecked = tonumber(arg_list[2])
      if delay_unchecked then
         delay = delay_unchecked
      end
      log(string.format("��������, ����� ������� �� ���� � ��: %d (�������)", delay))
   end,
   help = function(_)
      log(string.format("%s <on/off> - ���������/�������� ������", CMD))
      log(string.format("%s <calc/ab> - ����������� ���� ��� �����������", CMD))
      log(string.format("%s delay <seconds> - ���������� �������� (����� �� �����)", CMD))
   end,
}

local function cmd_execute(args)
   local arg_list = {}
   for token in args:gmatch("[^%s]+") do 
      table.insert(arg_list, token) 
   end
   cmds[arg_list[1]](arg_list)
end
 
local function calc(str)
   return assert(load("return " .. str))()
end

local function replace_ab(text)
   local replace = {
      [65] = 225,  -- A (latin) -> �
      [97] = 225,  -- a (latin) -> �
      [192] = 225, -- A (cyrillic) -> �
      [224] = 225, -- a (cyrillic) -> �
      [193] = 224, -- � -> � (cyrillic)
      [225] = 224, -- � -> a (cyrillic)
   }
   
   local result = ""

   for i = 1, #text do
      local char = text:byte(i)
      for old, new in pairs(replace) do
         if char == old then
            char = new
            break
         end
      end
      result = result .. string.char(char)
   end
   
   return result
end

local function is_in_array(array, value)
   for _, element in ipairs(array) do
      if value == element then
         return true
      end
   end
   return false
end

local answer_thr = lua_thread.create_suspended(
function(text)
   _wait(300)
   local clown_id = text:match(SN_pattern)
   local sequence = text:match('"(.-)"')
   if clown_id and sequence then
      local func = mode == "calc" and calc or replace_ab
      local final_cmd = string.format(PM_pattern, clown_id, func(sequence))
      sampSendChat(final_cmd)
      -- debug: print(text, final_cmd)
   end
end)

ev.onServerMessage = function(_, text)
   if state then
      answer_thr:run(text)
   end
end

function main()
	while not isSampAvailable() do wait(0) end
   
   do
      local ip, _ = sampGetCurrentServerAddress()
      if not is_in_array(GALAXY_IP, ip) then
         thisScript():unload()
      end
   end
   --[[
   do -- debug
      local test1 = '������� �� vika_evseeva666[322]: :)))))) "�������������"'
      local test2 = '������� �� pidarockxgod[228]: � ��������� ������� ����������� ������� ������� ������� "28*322-10-20"'
      
      mode = "ab"
      answer_thr:run(test1)
      
      _wait(delay * 2)

      mode = "calc"
      answer_thr:run(test2)
   end
   ]]

   print("/automp help - ������ ������")
   sampRegisterChatCommand(CMD, cmd_execute)

   wait(-1)
end

