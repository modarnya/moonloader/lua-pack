-- change encoding to UTF8 before use

-- config
local PASSWORD = "ngogo123xxx"; -- required: length > 6
--

-- const
local AUTH_TITLE = "Авторизация";
local REGISTER_TITLE = "Регистрация";
local SELECT_GENDER_TITLE = "Пол";
local PROMOCODE_TITLE = "Промо%-код";
local RULES_TITLE = "Общие правила";
--

require("addon");
local events = require("samp.events");

function events.onShowDialog(dialogId, _, title)
      if title:find(AUTH_TITLE) or title:find(REGISTER_TITLE) then
            sendDialogResponse(dialogId, 1, 0, PASSWORD);
            return false;
      end

      if title:find(SELECT_GENDER_TITLE) then
            sendDialogResponse(dialogId, 0, 0, "");
            return false;
      end

      if title:find(PROMOCODE_TITLE) then
            sendDialogResponse(dialogId, 0, 0, "");
            return false;
      end

      if title:find(RULES_TITLE) then
            sendDialogResponse(dialogId, 1, 0, "");
            return false;
      end

      return true;
end
