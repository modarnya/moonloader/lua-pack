script_name("drop_shit")
script_author("THERION")
script_dependencies("SAMPFUNCS")

-- lib
require("sampfuncs")
local weapons = require("game.weapons")
--

-- config
local isScriptEnabled = true
local toggleScriptCommand = "dropshit"
local weaponsToBeDropped = {
   weapons.GOLFCLUB,
   weapons.BASEBALLBAT,
   weapons.POOLCUE,
   weapons.KATANA,
   weapons.SPRAYCAN,
}
--

for _, weapon in ipairs(weaponsToBeDropped) do
   weaponsToBeDropped[weapon] = true
end

function onReceiveRpc(packetID, bitStream)
   if not isScriptEnabled then
      return true, packetID, bitStream
   end
   
   if packetID ~= RPC_SCRGIVEPLAYERWEAPON then
      return true, packetID, bitStream
   end

   local weaponID = raknetBitStreamReadInt32(bitStream)
   if weaponsToBeDropped[weaponID] then
      return false
   end

   return true, packetID, bitStream
end

function main()
   repeat wait(0) until isSampAvailable()

   sampRegisterChatCommand(toggleScriptCommand, function()
      isScriptEnabled = not isScriptEnabled
      local scriptStateAsString = isScriptEnabled and "enabled" or "disabled"
      local message = ("~h~%s~w~: %s"):format(script.this.name, scriptStateAsString)
      printStyledString(message, 0x1000, 4)
   end)
end
