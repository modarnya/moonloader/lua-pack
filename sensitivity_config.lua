script_name("sensitivity_config")
script_dependencies("mimgui")

-- lib
require("moonloader")
local memory = require("memory")
local inicfg = require("inicfg")
local imgui  = require("mimgui")
--

-- const
local TOGGLE_IMGUI_MENU_BINDING = VK_DELETE
local IMGUI_INPUT_WIDTH = 160

local CAMERA_POINTER = 0x00B6F028

local THIRD_PERSON_AIMING_MODES = {
   [0x35] = "MODE_AIMWEAPON",
   [0x37] = "MODE_AIMWEAPON_FROMCAR",
   [0x41] = "MODE_AIMWEAPON_ATTACHED",
}

local FIRST_PERSON_AIMING_MODES = {
   [0x7] = "MODE_SNIPER",
   [0x8] = "MODE_ROCKETLAUNCHER",
   [0x33] = "MODE_ROCKETLAUNCHER_HS",
}
--

local function getDefaultConfig()
   local config = {
      default = 0.71653,
      aiming3rd = 0.400000,
      aiming1st = 0.400000,
   }

   return { main = config }
end

local function deserializeConfig(data)
   local config = {
      default = imgui.new.float(data.main.default),
      aiming3rd = imgui.new.float(data.main.aiming3rd),
      aiming1st = imgui.new.float(data.main.aiming1st),
   }
   
   return config
end

local function serializeConfig(data)
   local config = {
      default = data.default[0],
      aiming3rd = data.aiming3rd[0],
      aiming1st = data.aiming1st[0],
   }

   return { main = config }
end

-- state
local config = inicfg.load(getDefaultConfig(), script.this.name .. ".ini")
local isWindowOpen = imgui.new.bool(false)
--

local function getCameraMode()
   return memory.getuint8(CAMERA_POINTER + 0x180, false)
end

local function setSensitivity(value)
   local valueFormated = value / 1000
	memory.setfloat(0xB6EC1C, valueFormated, false)
	memory.setfloat(0xB6EC18, valueFormated, false)
end

local function isImGuiWindowOpen()
   return isWindowOpen[0]
end

local function onDrawImGuiFrame()
   imgui.Begin("Sensitivity", isWindowOpen, imgui.WindowFlags.AlwaysAutoResize)

   imgui.PushItemWidth(IMGUI_INPUT_WIDTH)

   imgui.Text("Default")
   imgui.InputFloat("##default", config.default, 1)
   imgui.Text("Aiming 3rd person")
   imgui.InputFloat("##aiming3rd", config.aiming3rd, 1)
   imgui.Text("Aiming 1st person")
   imgui.InputFloat("##aiming1st", config.aiming1st, 1)

   if imgui.Button("Save", imgui.ImVec2(IMGUI_INPUT_WIDTH, 0)) then
      config = serializeConfig(config)
      inicfg.save(config, script.this.name .. ".ini")
      config = deserializeConfig(config)
   end

   imgui.PopItemWidth()

   imgui.End()
end

local function onInit()
   if not doesFileExist(script.this.name .. ".ini") then 
      inicfg.save(config, script.this.name .. ".ini")
   end
   config = deserializeConfig(config)

   imgui.OnInitialize(imgui.StyleColorsLight)
   imgui.OnFrame(isImGuiWindowOpen, onDrawImGuiFrame)
end

local function onEveryFrame()
   if isKeyJustPressed(TOGGLE_IMGUI_MENU_BINDING) then
      isWindowOpen[0] = not isWindowOpen[0]
   end

   local sensitivity = config.default[0]

   local cameraMode = getCameraMode()
   
   if FIRST_PERSON_AIMING_MODES[cameraMode] then
      sensitivity = config.aiming1st[0]
   end

   if THIRD_PERSON_AIMING_MODES[cameraMode] then
      sensitivity = config.aiming3rd[0]
   end

   setSensitivity(sensitivity)
end

function main()
   onInit()

   while true do
      wait(0)
      onEveryFrame()
   end
end