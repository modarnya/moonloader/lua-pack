script_name("fuck_hats_on_players")
script_author("THERION")

require("sampfuncs")

local FORBIDDEN_BONE_ID_LIST = {
   [1] = true,
   [2] = true,
   [15] = true,
   [16] = true,
   [17] = true,
   [18] = true,
}

function onReceiveRpc(packetId, bitStream)
   if packetId ~= RPC_SCRSETPLAYERATTACHEDOBJECT then
      return true, packetId, bitStream
   end

   local playerId = raknetBitStreamReadInt16()
   raknetBitStreamIgnoreBits(bitStream, 32)
   local isCreatingObject = raknetBitStreamReadBool(bitStream)
   if not isCreatingObject then
      return true, packetId, bitStream
   end

   local isLocalPlayerAvailable, localPlayerId = sampGetPlayerIdByCharHandle(PLAYER_PED)

   if isLocalPlayerAvailable and localPlayerId == playerId then
      return true, packetId, bitStream
   end

   raknetBitStreamIgnoreBits(bitStream, 32)
   local boneId = raknetBitStreamReadInt32(bitStream)

   if FORBIDDEN_BONE_ID_LIST[boneId] then
      return false
   end

   return true, packetId, bitStream
end