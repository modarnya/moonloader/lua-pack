local ffi = require("ffi")

ffi.cdef[[
   int __stdcall GetVolumeInformationA(
      const char* lpRootPathName,
      char* lpVolumeNameBuffer,
      uint32_t nVolumeNameSize,
      uint32_t* lpVolumeSerialNumber,
      uint32_t* lpMaximumComponentLength,
      uint32_t* lpFileSystemFlags,
      char* lpFileSystemNameBuffer,
      uint32_t nFileSystemNameSize
   );
]]

local function onRequestCode()
   local serialNumber = ffi.new("unsigned long[1]", 0)
   ffi.C.GetVolumeInformationA(nil, nil, 0, serialNumber, nil, nil, nil, 0)
   local code = serialNumber[0]

   if code == 0 then
      sampAddChatMessage("Error occured when trying to get the code", -1)
      return
   end

   setClipboardText(code)
   sampAddChatMessage(("Code: %d. Copied to clipboard."):format(code), -1)
end

function main()
   repeat wait(0) until isSampAvailable()
   sampRegisterChatCommand("key", onRequestCode)
   wait(-1)
end