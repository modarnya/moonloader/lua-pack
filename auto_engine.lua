script_name("auto_engine")
script_authors("Roman1us", "THERION")

-- lib
local samp_events = require("samp.events")
local encoding = require("encoding")
encoding.default = "CP1251"
--

-- const
local RESTART_DELAY = 60
local START_ENGINE_COMMAND = "/engine"
local TRIGGER_MESSAGES = {
   encoding.UTF8:decode("^Чтобы завести транспорт, набери %{33AA33%}%/engine%{FFFFFF%} или нажми клавишу %{33AA33%}NUM 4%{FFFFFF%}.$"),
   encoding.UTF8:decode("^Двигатель заглох.$"),
   encoding.UTF8:decode("^Транспорт сильно повреждён. Набери %/mechanic для вызова механика, либо %/taxi для вызова такси.$"),
   encoding.UTF8:decode("^Чтобы закрыть транспорт от воров, набери %/lock.$"),
   encoding.UTF8:decode("^Отлично. Отправляйся в точку, отмеченную на карте.$"),
}
--

local function start_engine() 
   wait(RESTART_DELAY) 
   sampSendChat(START_ENGINE_COMMAND) 
end

function on_server_message(color, text)
   if not isCharInAnyCar(PLAYER_PED) then
      return
   end

   for _, element in ipairs(TRIGGER_MESSAGES) do
      if text:find(element) then
         lua_thread.create(start_engine)
         return
      end
   end
end

local function on_init()
   function samp_events.onServerMessage(color, text)
      on_server_message(color, text)
   end
end

function main()
   repeat wait(0) until isSampAvailable()
   on_init()
end