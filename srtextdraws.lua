local ev = require("samp.events")

local SR_CHEATING_IP = "51.15.205.209"
local forbidden = {"FPS", "%d"}

function main()
   repeat wait(0) until isSampAvailable()

   do 
      local ip, _ = sampGetCurrentServerAddress()
      if ip == SR_CHEATING_IP then -- check if SR_Team Cheating
         ev.onShowTextDraw = function(textdraw_id, data)
            for _, pattern in ipairs(forbidden) do
               if data.text:find(pattern) then
                  return false
               end
            end 
         end
      else
         thisScript():unload()
      end
   end
   wait(-1)
end