script_description("Disables n-tags on command")

local memory = require("memory")

-- prints message to SAMP Chat
local function log(msg)
   local format = "{FB4343}[%s]{FFFFFF}: %s{FFFFFF}."
   sampAddChatMessage(string.format(format, thisScript().name, msg), -1)
end

function main()
   repeat wait(0) until isSampAvailable()
   
   local sptr = sampGetServerSettingsPtr()

   sampRegisterChatCommand("disnt",
   function()
      local value = memory.getint8(sptr + 56)
      if value ~= 0 then
         memory.setint8(sptr + 56, 0)
         log("ON", 3000)
      else
         memory.setint8(sptr + 56, 1)
         log("OFF", 3000)
      end
   end)

   wait(-1)
end