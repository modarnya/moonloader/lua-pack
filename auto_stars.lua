-- be careful, encoding is CP1251
script_author("THERION")
script_description("����������� ������� ��������� �� ������ ��� ���� ������")
script_dependencies("SAMP.lua", "mimgui", "tabler_icons")

--- this data should be correct for proper work 
--- and should be updated in case of server changes
local EMT_SKINS = {70, 156, 275, 274, 276, 308}
local DIALOG_TITLE = "�����"
local CMD_PATTERN = "/frisk (%d+)"
local ADD_WANTED_PATTERN = "/su %d %d %s"
local FORBIDDEN_STUFF = {[0] = "�����", "���������", "����� �������"}
---


local BUFFER_SIZE = 64
-- max string size
local CMD = string.lower(thisScript().name)
-- settings menu command


local function default()
   -- default settings table
   return {
      frisk = {
         on = true,
         cd_su = 0.5,
         cd_screen = 0.05,
         screen = false,
         message = "���������� ����",
      }
   }
end

-- prints out if user does not have some of the needed files
local function catch_exception(status, path)
   local msg = string.format('File not found or has errors: "%s". Shutting down..', path)
   assert(status, msg)
end

do -- Loading libraries
   local list = {
      ffi        = "ffi",
      inicfg     = "inicfg",
      sampev     = "samp.events",
      imgui      = "mimgui",
      encoding   = "encoding",
      ti         = "tabler_icons",
   }

   local result = nil
   for var, path in pairs(list) do
      result, _G[var] = pcall(require, path)
      catch_exception(result, path)
   end
end

local ini = inicfg.load(default(), thisScript().name .. ".ini")

local u8 = encoding.UTF8
encoding.default = "CP1251"

local win_state = imgui.new.bool(false)
-- imgui window state
local last_victim_id = nil 
-- contains id of last interacted player

-- check if value is in array
local function is_in_array(array, value)
   for _, element in ipairs(array) do
      if value == element then
         return true
      end
   end
   return false
end
-- wait for seconds
local function _wait(time)
   local countdown = os.clock() + time 
   while os.clock() < countdown do 
      wait(0) 
   end
end
-- check if player has emt skin
local function is_player_medic(player_id)
   local _, handle = sampGetCharHandleBySampPlayerId(player_id)
   local model = getCharModel(handle)
   return is_in_array(EMT_SKINS, model)
end

sampev.onShowDialog = function(_, _, title, _, _, text)
   if ini.frisk.on[0] and title:find(DIALOG_TITLE) then
      lua_thread.create(
      function()
         local stars = {}

         for idx, pattern in ipairs(FORBIDDEN_STUFF) do
            if text:find(pattern) then
               local emt_check = idx == 0 and is_player_medic(last_victim_id)
               if not emt_check then
                  stars = stars + 1
               end
            end
         end

         -- wait for dialog to init properly
         _wait(ini.frisk.cd_screen[0])

         if ini.frisk.screen[0] then -- make screenshot
            callFunction(sampGetBase() + 0x70FC0, 0, 0) 
         end
         

         sampCloseCurrentDialogWithButton(0)
         _wait(ini.frisk.cd_su[0])
         
         if stars > 0 then
            local reason = u8:decode(ffi.string(ini.frisk.message))
            local cmnd = string.format(ADD_WANTED_PATTERN, last_victim_id, stars, reason)
            sampSendChat(cmnd) 
         end
      end)
   end
end

sampev.onSendCommand = function(cmd)
   local id = cmd:match(CMD_PATTERN)
   if ini.frisk.on[0] and id then
      last_victim_id = id
   end
end

function main()
	do -- save config
      if not doesDirectoryExist("moonloader\\config") then 
         createDirectory("moonloader\\config") 
      end

      if not doesFileExist(thisScript().name .. ".ini") then 
         inicfg.save(ini, thisScript().name .. ".ini") 
      end
   end

	repeat wait(0) until isSampAvailable()

   imgui.to()

   sampRegisterChatCommand(CMD, 
   function()
      win_state[0] = not win_state[0]
   end)
      
   wait(-1)
end

local function load_icons(font_size)
   local config = imgui.ImFontConfig()
   config.MergeMode = true
   config.PixelSnapH = true
   local iconRanges = imgui.new.ImWchar[3](ti.min_range, ti.max_range, 0)
   imgui.GetIO().Fonts:AddFontFromMemoryCompressedBase85TTF(ti.get_font_data_base85(), font_size, config, iconRanges)
end

imgui.OnInitialize(function()
   load_icons(15)
   imgui.GetIO().IniFilename = nil
end)

local new_frame = imgui.OnFrame(
function() 
   return win_state[0] 
end,
function(player)
   imgui.apply_style()

   imgui.SetNextWindowPos(imgui.ImVec2(40, 275), imgui.Cond.FirstUseEver)
   imgui.Begin(thisScript().name, _, imgui.WindowFlags.NoResize + imgui.WindowFlags.NoTitleBar + imgui.WindowFlags.AlwaysAutoResize)
      local white  = imgui.ImVec4(1, 1, 1, 1)
      local orange = imgui.ImVec4(1, 0.32, 0, 1)

      imgui.Link(thisScript().url, ti("brand-gitlab"), white, orange)

      imgui.SameLine(140)

      imgui.Text(thisScript().name)

      -- settings
      imgui.Checkbox("Enabled", ini.frisk.on)
      
      imgui.SliderFloat("Command delay", ini.frisk.cd_su, 0, 2)
      imgui.SliderFloat("Screenshot delay", ini.frisk.cd_screen, 0, 1)
      imgui.InputText("Wanted reason", ini.frisk.message, BUFFER_SIZE)
      -- settings

      local btn_size = imgui.ImVec2(108, 20)
      if imgui.Button("Save", btn_size) then
         imgui.from()
         inicfg.save(ini, thisScript().name .. ".ini")
         imgui.to()
      end
      imgui.SameLine()
      if imgui.Button("Default", btn_size) then
         ini = default()
         imgui.to()
      end
   imgui.End()
end)

-- clickable text, good stuff
imgui.Link = function(link, name, color, color_hovered)
   local size = imgui.CalcTextSize(name)
   local p = imgui.GetCursorScreenPos()
   local p2 = imgui.GetCursorPos()
   local result_btn = imgui.InvisibleButton("##".. link .. name, size)

   if result_btn then
      os.execute("explorer ".. link)
   end

   imgui.SetCursorPos(p2)
   if imgui.IsItemHovered() then
      imgui.TextColored(color_hovered, name)
   else
      imgui.TextColored(color, name)
   end
   
   return result_btn
end

--- looks like shit for me but when not much functionality it feels ok
imgui.to = function()
   local info = ini.frisk
   local new  = imgui.new

   info.on           = new.bool(info.on)
   info.screen       = new.bool(info.screen)
   info.cd_su        = new.float(info.cd_su)
   info.cd_screen    = new.float(info.cd_screen)
   info.message      = new.char[BUFFER_SIZE](u8(info.message))
end

imgui.from = function()
   local info = ini.frisk

   info.on           = info.on[0]
   info.screen       = info.screen[0]
   info.cd_su        = info.cd_su[0]
   info.cd_screen    = info.cd_screen[0]
   info.message      = u8:decode(ffi.string(info.message))
end

-- shitcode section end

-- imgui style
imgui.apply_style = function()
   -- thx man with a moon profile picture
   imgui.SwitchContext()
   local colors = imgui.GetStyle().Colors
   colors[imgui.Col.Text]                    = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
   colors[imgui.Col.TextDisabled]            = imgui.ImVec4(1.00, 1.00, 1.00, 0.20)
   colors[imgui.Col.SliderGrab]              = imgui.ImVec4(0.90, 0.90, 0.90, 1.00)
   colors[imgui.Col.SliderGrabActive]        = imgui.ImVec4(0.70, 0.70, 0.70, 1.00)
   colors[imgui.Col.ScrollbarBg]             = imgui.ImVec4(0.60, 0.60, 0.60, 0.90)
   colors[imgui.Col.ScrollbarGrab]           = imgui.ImVec4(0.90, 0.90, 0.90, 1.00)
   colors[imgui.Col.ScrollbarGrabHovered]    = imgui.ImVec4(0.80, 0.80, 0.80, 1.00)
   colors[imgui.Col.ScrollbarGrabActive]     = imgui.ImVec4(0.70, 0.70, 0.70, 1.00)
   colors[imgui.Col.FrameBg]                 = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
   colors[imgui.Col.FrameBgHovered]          = imgui.ImVec4(0.20, 0.20, 0.20, 0.80)
   colors[imgui.Col.FrameBgActive]           = imgui.ImVec4(0.20, 0.20, 0.20, 0.60)
   colors[imgui.Col.CheckMark]               = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
   colors[imgui.Col.Button]                  = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
   colors[imgui.Col.ButtonHovered]           = imgui.ImVec4(0.15, 0.15, 0.15, 1.00)
   colors[imgui.Col.ButtonActive]            = imgui.ImVec4(0.10, 0.10, 0.10, 1.00)
   colors[imgui.Col.TextSelectedBg]          = imgui.ImVec4(0.80, 0.80, 0.80, 0.80)
end